# Analysis of News engagement on Facebook from credible and non-credible news sources.

This project studies the engagement of news articles shared on Facebook, from both credible and non-credible sources.
